<?php
require 'config.php';
require 'dao/ProdutosDaoMySql.php';
include 'header.php';
include 'footer.php';
$produtoDao= new ProdutosDaoMySql($pdo);

$id= filter_input(INPUT_POST,'id');
$nome = filter_input(INPUT_POST, 'name');
$quantidade = filter_input(INPUT_POST,'quantidade');
$valor = filter_input(INPUT_POST,'valor');
$categoriaProduto = filter_input(INPUT_POST,'categoria_produto');

if($id && $name && $email){
    $produto=$produtoDao->findById($id);
    $produto->setNome($nome);
    $produto->setQuantidade($id);
    $produto->setValor($valor);
    $produto->setCampoCategoria($categoriaProduto);

    $produtoDao->update($produto);

    header("location: index.php");
    exit;
}else{
    header("Location: editar.php?id=".$id); 
    exit;
}
