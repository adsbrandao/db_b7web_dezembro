<?php
require_once 'models/TbMonetaria.php';


class TbMonetariaDaoMySql implements TbMonetariaDao{
    private $pdo;
    public function __construct(PDO $driver)
    {
        $this->pdo=$driver;
    }
    public function add(TbMonetaria $l){
        $sql=$this->pdo->prepare("INSERT INTO tb_monetaria(nome,tipo_pagamento,numerodois) VALUES(:nome,:tipo_pagamento,:numerodois)");
        $sql->bindValue(':nome',$l->getNome());
        $sql->bindValue(':nome',$l->getTipoPagamento());
        $sql->bindValue(':nome',$l->getNumeroDois());
        $sql->execute();

        $l->setId( $this->pdo->lastInsertId() );
        return $l;
    }
    public function update(TbMonetaria $l){
        $sql=$this->pdo->prepare("UPDATE tb_monetaria SET nome=:nome WHERE id=:id");
        $sql->bindValue(':nome',$l->getNome());
        $sql->bindValue(':id',$l->getId());
        $sql->execute();

        return true;
    }
    public function delete($id){
        $sql=$this->pdo->prepare("DELETE FROM tb_monetaria WHERE id=:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
    }
    public function findAll(){
        $array=[];
        $sql=$this->pdo->query("SELECT * FROM tb_monetaria");
        if($sql->rowCount() > 0){
            $data=$sql->fetchAll();

            foreach($data as $item){
                $t = new TbMonetaria();
                $t->setId($item['id']);
                $t->setNome($item['nome']);
                $t->setTipoPagamento($item['tipo_pagamento']);
                $t->setNumeroDois($item['numero_dois']);
                $array[] = $t;
            }
        }
        return $array;
    }
    public function findById($id){
        $sql=$this->pdo->prepare("SELECT * FROM tb_monetaria WHERE id =:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $t = new TbMonetaria();
            $t->setId($data['id']);
            $t->setNome($data['nome']);
            $t->setTipoPagamento($data['tipo_pagamento']);
            $t->setNumeroDois($data['numero_dois']);
            return $t;
        }else{
            return false;
        }
    }
    public function findByNome($nome){
        $sql=$this->pdo->prepare("SELECT * FROM tb_monetaria WHERE nome =:nome");
            $sql->bindValue(':nome',$nome);
            $sql->execute();
            if($sql->rowCount() > 0){
                $data=$sql->fetch();
    
                $t = new TbMonetaria();
                $t->setId($data['id']);
                $t->setNome($data['nome']);
                $t->setTipoPagamento($data['tipo_pagamento']);
                $t->setNumeroDois($data['numero_dois']);
                return $t;
            }else{
                return false;
            }
    }
}
?>