<?php
 require 'config.php';

class TbMonetaria
{
    
    private $id;
    private $nome;
    private $tipo_pagamento;
    private $numero_dois;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getNome(){
        return $this->nome;
    }
    public function setNome($i){
        $this->nome=trim($i);
    }
    public function getTipoPagamento(){
        return $this->tipo_pagamento;
    }
    public function setTipoPagamento($i){
        $this->tipo_pagamento=trim($i);
    }
    public function getNumeroDois(){
        return $this->numero_dois;
    }
    public function setNumeroDois($i){
        $this->numero_dois=trim($i);
    }
}
interface TbMonetariaDao{
    public function add(TbMonetaria $l);
    public function update(TbMonetaria $l);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findByNome($nome);
}
?>

