<?php
require 'config.php';
require 'dao/TbMonetariaDaoMySql.php';

$tbMonetariaDao=new TbMonetariaDaoMySql($pdo);

$nome = filter_input(INPUT_POST, 'nome');
$tipo_pagamento=filter_input(INPUT_POST,'tipo_pagamento');
$numero_dois=filter_input(INPUT_POST,'numero_dois');

if($nome && $tipo_pagamento && $numero_dois){
    if($tbMonetariaDao->findByNome($nome)=== false){
        $novoMonetaria= new TbMonetariaDao();
        $novoMonetaria->setNome($nome);
        $novoMonetaria->setTipoPagamento($tipo_pagamento);
        $novoMonetaria->setNumeroDois($numero_dois);
        // $novoMonetaria->setCampoCategoria($campo_categoria);

        $tbMonetariaDao-> add($novoMonetaria);

        header("Location: index.php");
        exit;
    }else{
        header("Location: adicionar.php");
        exit;
    }
}else{
    header("Location: adicionar.php"); 
    exit;
}
